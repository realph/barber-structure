<?php
// Use these settings on the local server
if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
  include( dirname( __FILE__ ) . '/wp-config-local.php' );
  
// Otherwise use the below settings (on live server)
} else {
 
  // Live Server Database Settings
  define( 'DB_NAME',     'database_name_here');
  define( 'DB_USER',     'username_name');
  define( 'DB_PASSWORD', 'password_here' );
  define('DB_HOST', $_ENV{DATABASE_SERVER});
  
  // Overwrites the database to save keep editing the DB
  define('WP_HOME','http://site_url_here.com');
  define('WP_SITEURL','http://site_url_here.com');
  
  // Turn Debug off on live server
  define('WP_DEBUG', false);
}
 
// Never use wp_ always use your own to prevent some hacks
$table_prefix  = 'project_name_here_';
 
// Usual Wordpress stuff - Dont overide the ones you have already
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');
define('AUTH_KEY',         'wordpress generated code');
define('SECURE_AUTH_KEY',  'wordpress generated code');
define('LOGGED_IN_KEY',    'wordpress generated code');
define('NONCE_KEY',        'wordpress generated code');
define('AUTH_SALT',        'wordpress generated code');
define('SECURE_AUTH_SALT', 'wordpress generated code');
define('LOGGED_IN_SALT',   'wordpress generated code');
define('NONCE_SALT',       'wordpress generated code');
define('CONCATENATE_SCRIPTS', false );
 
define('WPLANG', '');
 
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');
        
require_once(ABSPATH . 'wp-settings.php');