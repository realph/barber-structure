<?php 
// Local server settings
 
// Local Database
define('DB_NAME', 'local_database_name_here');
define('DB_USER', 'local_username_here');
define('DB_PASSWORD', 'local_password_here');
define('DB_HOST', 'localhost');
 
// Overwrites the database to save keep editing the DB
define('WP_HOME','http://dev.project_name_here.com');
define('WP_SITEURL','http://dev.project_name_here.com');