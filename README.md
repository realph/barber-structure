The Barber Structure - An easy folder structure for Wordpress deployments via Git
=================================================================================

Based on Roy Barber's Version Controlling WordPress [article](http://roybarber.com/version-controlling-wordpress/).

How to use:

1. Unzip directory.
2. Place .gitignore, html and sql folders in your project directory alongside your .git directory.
3. Rename the wp-config-sample.php file to wp-config.php.
4. Rename the wp-config-local-sample.php file to wp-config-local.php.
5. Open both config files in a text editor and fill in your database details.

Note: Try and leave the sql/DONTDELETE.md file alone, it keeps the sql folder tracked.
